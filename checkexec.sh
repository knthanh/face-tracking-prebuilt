#!/bin/sh

for script in "$@"
do

PID=$(pgrep -f '$script')

echo $PID

if [ -z "${PID}" ]; then
	echo "$script is not running" 
else
	echo "$script Running"
fi
done
