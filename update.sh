#!/bin/sh

git checkout master
git remote update

UPSTREAM=${1:-'@{0}'}
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
    echo "Up to date"

elif [ $LOCAL = $BASE ]; then
	echo "Need to update"
	echo "Updating..."
	git pull --all
else
    echo "Diverged"
fi